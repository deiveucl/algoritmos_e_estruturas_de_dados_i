class CalculadoraInt {
    private int operando_1;
    private int operando_2;

    public CalculadoraInt() {
        operando_1 = 1;
        operando_2 = 1;
    }

    public CalculadoraInt(int oper_1, int oper_2) {
        operando_1 = oper_1;
        operando_2 = oper_2;
    }

    public void SetOperando_1(int oper_1) {
        operando_1 = oper_1;
    }
    public void SetOperando_2(int oper_2) {
        operando_2 = oper_2;
    }

    public int GetSoma() {
        return operando_1 + operando_2;
    }
    public int GetSubtracao() {
        return operando_1 - operando_2;
    }
    public float GetDivisao() {
        return operando_1 / operando_2;
    }
    public int GetMultiplicacao() {
        return operando_1 * operando_2;
    }

    public int GetMaior() {
        if (operando_1 > operando_2) {
            return operando_1;
        } else {
            return operando_2;
        }
    }

    public static int Soma(int oper_1, int oper_2) {
        return oper_1 + oper_2;
    }

    public static bool Primo(int numero) {
        int cont_divisores = 0;
        int divisor = numero;

        while (divisor != 0) {
        if (numero % divisor == 0) {
            cont_divisores++;
        }

        if (divisor > 0) {
            divisor--;
        } else if (divisor < 0) {
            divisor++;
        }
        }

        if (cont_divisores == 2) {
            return true;
        }
            return false;
    }
    
}