﻿using System;

class Program {

    static void Main(string[] args) {
        
            
        // Instanciando a classe em um objeto
        CalculadoraInt calc_vermelha = new CalculadoraInt(5, 10);

        // Testando métodos soma, subtração, divisão e multiplicação
        Console.WriteLine("\nA soma é igual a {0}", calc_vermelha.GetSoma());
        Console.WriteLine("A subtração é igual a {0}", calc_vermelha.GetSubtracao());
        Console.WriteLine("A divisão é igual a {0}", calc_vermelha.GetDivisao());
        Console.WriteLine("A multiplicação é igual a {0}\n", calc_vermelha.GetMultiplicacao());

        // Teste de utilização do método não estático maior
        for (int i = 6; i < 15; i++) {
            calc_vermelha.SetOperando_1(i);
            Console.WriteLine("O maior é valor é {0}", calc_vermelha.GetMaior());
        }

        // Apenas para pular linha
        Console.WriteLine();

        // Teste de utilização do método estático SOMA
        for (int i = -5; i < 5; i++) {
            Console.Write("[{0}] ", CalculadoraInt.Soma(1, i));
        }
        
        Console.WriteLine("Insira apenas números prímos: ");

        // Teste da função estática Primo
        int somaTotal = 0;
        while (somaTotal < 200) {
            bool verificador = false;
            int numUsuario = int.Parse(Console.ReadLine());
            if (CalculadoraInt.Primo(numUsuario)) {
                somaTotal += numUsuario;
                verificador = true;
            }
            Console.WriteLine("{0}", verificador);
        }
        Console.WriteLine("A soma total é igual a {0}. \nSaindo do loop...", somaTotal);

    }

}