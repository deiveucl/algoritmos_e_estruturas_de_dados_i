<h2>Projeto da Disciplina Algoritmos e Estruturas de Dados - UCL, 02/2019</h2>

<h3>Gerenciamento e Consulta à Informações de um Museu</h3>

<p>Nosso projeto tem a intenção de prover um sistema para consulta de informações sobre o museu e gerencia do mesmo</p>

<h3>O usuário comum pode:</h3>
<ul>
  <li><b>Agendar visita</b></li>
  <li><b>Ver informações sobre o Museu:</b></li>
  <ul>
    <li>Data de inauguração</li?
    <li>Fundadores</li>
    <li>Quantidade de obras em exposição</li>
    <li>Nome do CEO atual</li>
    <li>Quantidade de colaboradores</li>
    <li>Tamanho em metros quadrados</li>
  </ul>
  
  <li><b>Listar as obras em exposição, na qual informará:</b></li>
  <ul>
    <li>Título da obra</li>
    <li>Nome do autor</li>
    <li>Data de sua criação</li>
    <li>Uma breve descrição</li>
  </ul>
  
  <li><b>Listar os artistas que já passaram pelo museu</b></li>
  <ul>
    <li>Nome do artista</li>
    <li>Data de seu nascimento</li>
    <li>Áreas de atuação</li>
  </ul>
  <li><b>Deixar um Feedback sobre sua visita ao museu </li></b>
</ul>

<h3>A gerência do museu terá acesso a:</h3>
<ul>
  <li><b>Alterar informações e caracteristicas do museu:</li></b>
  <ul>
    <li>Presidência do Museu</li>
    <li>Quantidade de funcionários </li>
    <li>Metros quadrados disponíveis aos visitantes </li>
  </ul>

  <li><b>Poderá alterar as exposições e atrações do museu:</li></b>
  <ul>
    <li>Cadastrar novos artisitas</li>
    <li>Cadatrar novas obras em exposição</li>
  </ul>
     
  <li><b>Terá acesso as interações e solicitações dos visitantes:</li></b>
  <ul>
    <li>Visualizar as visitas agendadas</li>
    <li>Visualizar as avaliações dos visitantes </li>
  </ul>
  
  <li><b>Poderá apagar os dados salvos nas bases de dados:</li></b>
  <ul>
    <li>Apagar os feedback's dos visitantes</li>
    <li>Apagar a lista de obras cadastradas</li>
    <li>Apagar os artisitas cadastrados</li>
    <li>Apagar a lista de visitas agendadas</li>
  </ul>
</ul>

<ul>
  <li><b>OBS:</b></li>
  <ul>
    <li>Usuários permitidos:</li>
    <ul>
      <li>kayc</li>
      <li>deivisson</li>
      <li>andre</li>
    </ul>
    <li>Senha:</li>
    <ul>
      <li>museumassa</li>
    </ul>
  </ul>
</ul>
