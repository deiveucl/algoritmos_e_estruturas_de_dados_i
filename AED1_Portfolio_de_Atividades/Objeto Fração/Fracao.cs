class Fracao {

    // Definindo meus atributos
    private int numerador;
    private int denominador;

    // Definindo o método construtor
    public Fracao(int numerador, int denominador) {
        this.numerador = numerador;
        this.denominador = denominador;
    }

    // Criando métodos GET
    public int getNumerador() {
        return this.numerador;
    }
    public int getDenominador() {
        return this.denominador;
    }
    public string getFracao() {
        return "Esta fração está definida como: " + numerador.ToString() + "/" + denominador.ToString();
    }

    // Criando o método SIMPLIFICAR
    public void setSimplificar() {

        // Variável para armazenar o menor valor
        int menorValor;

        // Descobrindo qual é o menor valor
        if (numerador < denominador || numerador == denominador) {
        menorValor = numerador;
        } else {
        menorValor = denominador;
        }
        
        // Estrutura de repetição para todas as possibilidades de simplificação
        int contador = 2;
        do {
        
        // Garante todas as possibilidades de divisão dinamicamente
        int divisor = contador;
        do {
            
            // Caso o numerador e denominador sejam divisíveis pelo divisor atual, devide
            if (numerador % divisor == 0 && denominador % divisor == 0) {
            numerador = numerador / divisor;
            denominador = denominador / divisor;
            } else {
            // Caso não seja, o divisor é incrementado + 1 para sair do loop
            divisor++;
            }
            
        } while (divisor == contador);

        // Incrementa o contador para tentar o próximo divisor
        contador++;

        } while (contador <= menorValor);
    
    }

}