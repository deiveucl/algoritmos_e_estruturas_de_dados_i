﻿using System;

class Program {
    static void Main(string[] args) {
            
        // Criando uma classe para instanciar um objeto FRAÇÃO
        // Criar o método: SIMPLIFICAR FRAÇÂO

        // Instanciando o objeto FRACAO
        Fracao minhaFracao = new Fracao(25, 5);

        // Pegando dados da FRACAO usando os métodos GET
        Console.WriteLine("\nNumerador: {0}", minhaFracao.getNumerador());
        Console.WriteLine("Denominador: {0}", minhaFracao.getDenominador());
        Console.WriteLine("{0}", minhaFracao.getFracao());
        
        // Simplificando minha FRACAO
        minhaFracao.setSimplificar();

        // Pegando dados da FRACAO usando os métodos GET depois da simplificação
        Console.WriteLine("\nNumerador: {0}", minhaFracao.getNumerador());
        Console.WriteLine("Denominador: {0}", minhaFracao.getDenominador());
        Console.WriteLine("{0}", minhaFracao.getFracao());
        
    }
}