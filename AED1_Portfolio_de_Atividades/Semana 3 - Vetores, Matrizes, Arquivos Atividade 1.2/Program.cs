﻿using System;
using System.IO;

class MainClass {
    public static void Main (string[] args) {

        // Crie um programa em C# que receba do usuário 2 números inteiros e, em seguida, realize a soma dos dois números e grave esse valor em um arquivo
        
        // Definindo as variáveis e recebendo dados do usuário
        Console.Write("Insira o 1º número: ");
        float n1 = float.Parse(Console.ReadLine());
        Console.Write("Insira o 2º número: ");
        float n2 = float.Parse(Console.ReadLine());

        // Exibindo a soma
        Console.WriteLine("A soma entre {0} e {1} é igual a {2}.", n1, n2, n1 + n2);

        // Escrevendo o resultado no arquivo
        StreamWriter sw = new StreamWriter("somas.txt");
        sw.WriteLine(n1 + n2);

        // Fechando acessosw.WriteLine(texto);
        sw.Close();
    }
}