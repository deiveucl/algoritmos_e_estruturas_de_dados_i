﻿using System;
using System.IO;
using System.Text;

class Program {
    static void Main(string[] args) {
        
        // Associando um objeto manipulador ao meu arquivo
        FileStream meuArquivo = new FileStream("dados.txt", FileMode.Open, FileAccess.Read);

        // Usa o arquivo associado como um fluxo de dados em memória
        StreamReader sr = new StreamReader(meuArquivo, Encoding.UTF8);

        // Declarando um vetor para armazenar os dois números
        float[] notas = new float[2];

        // Lendo os dois números
        notas[0] = float.Parse(sr.ReadLine());
        notas[1] = float.Parse(sr.ReadLine());

        // Fechando o arquivo
        sr.Close();
        meuArquivo.Close();

        // Exibindo o resultado
        Console.WriteLine("Soma: {0} + {1} = {2}", notas[0], notas[1], notas[0] + notas[1]);

    }
}