﻿using System;

class Program {
    static void Main(string[] args) {

        /*
        Fazer um programa em C# para ler um vetor de inteiros positivos de 20 posições. Imprimir a quantidade de números pares e de múltiplos de 5.
        */

        // Fazendo a declaração do vetor inteiro com 20 posições
        int[] numerosInteiro = new int[20];

        // Preenchendo o vetor com dados do usuário
        for (int i = 0; i < 20; i++) {
        Console.Write("Insira o {0}º valor: ", i + 1);
            numerosInteiro[i] = int.Parse(Console.ReadLine());
        }

        // Inicializando as variáveis contadoras
        int contadorPares = 0;
        int contadorMultiCinco = 0;

        // Fazendo os cálculos
        for (int i = 0; i < 20; i++) {
            // Caso o resto da divisão por 2 seja 0, é PAR
            if (numerosInteiro[i] % 2 == 0) {
                contadorPares++;
            }

            // Caso o resto da divisão por 5 seja 0, é múltiplo de 5
            if (numerosInteiro[i] % 5 == 0) {
                contadorMultiCinco++;
            }
        }

        // Exibindo resultados
        Console.WriteLine("Existem {0} números pares no vetor", contadorPares);
        Console.WriteLine("Existem {0} múltiplos de cinco no vetor", contadorMultiCinco);

    }
}

