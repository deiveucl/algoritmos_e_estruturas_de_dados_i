﻿using System;

class Program {
    static void Main(string[] args) {
         
        // Declarando um array para armazenar os dígitos
        int[] digitos = new int[8];

        // Declarando uma variável para armazenar separadamente o inputs
        int digito;

        // Preenchendo o array com inputs do usuário
        for (int i = 0; i < 8; i++) {
        Console.Write("Escreva a {0}º dígito: ", i + 1);
        digito = int.Parse(Console.ReadLine());
        digitos[i] = digito;
        }

        // Juntando os dígitos em uma variável
        string matricula = "";
        for (int i = 0; i < 8; i++) {
        matricula = matricula + digitos[i].ToString();
        }

        // Calculando o dígito verificador
        int digitoVerificador = (digitos[0] * 2 + digitos[1] * 3 + digitos[2] * 4 + digitos[3] * 3 + digitos[4] * 2 + digitos[5] + digitos[6] + digitos[7]) % 10;

        // Inserindo o dígito verificador na matrícula
        matricula = matricula + "-" + digitoVerificador;

        // Mostrando a matrícula completa
        Console.WriteLine("A matrícula completa com o dígito verificador é: {0}", matricula);

    }
}
