﻿using System;
using System.IO;
using System.Text;

class MainClass {
    public static void Main(string[] args) {
        
        // Crie um programa em C# que realize a leitura de 20 letras presentes em um arquivo e, em seguida, exiba para o usuário o total de vogais, o total de consoantes e o total de dígitos
        
        // Associando um objeto manipulador ao meu arquivo
        FileStream meuArquivo = new FileStream("letras.txt", FileMode.Open, FileAccess.Read);

        // Usa o arquivo selecionado como um fluxo de dados em memória
        StreamReader sr = new StreamReader(meuArquivo, Encoding.UTF8);

        // Contador de letras, vogais e consoantes
        int qtdVogais = 0, qtdConsoantes = 0, qtdChar = 0;


        // Percorre cada linha do arquivo lendo as letras de cada linha
        while (!sr.EndOfStream) {

            // Armazena o valor em uma variável
            string temp = sr.ReadLine();
            
            // Incrementa o total de caracteres lidos
            qtdChar++;

            // Verifica se é vogal
            if (temp.ToLower() == "a" || temp.ToLower() == "e" || temp.ToLower() == "i" || temp.ToLower() == "o" || temp.ToLower() == "u") {
                qtdVogais++;
            } else if (temp.ToLower() == "b" || temp.ToLower() == "c" || temp.ToLower() == "d" || temp.ToLower() == "f" || temp.ToLower() == "g" || temp.ToLower() == "h" || temp.ToLower() == "j" || temp.ToLower() == "k" || temp.ToLower() == "l" || temp.ToLower() == "m" || temp.ToLower() == "n" || temp.ToLower() == "p" || temp.ToLower() == "q" || temp.ToLower() == "r" || temp.ToLower() == "s" || temp.ToLower() == "t" || temp.ToLower() == "v" || temp.ToLower() == "w" || temp.ToLower() == "x" || temp.ToLower() == "y" || temp.ToLower() == "z") {
                qtdConsoantes++;
            }

        }

        // Fecha o arquivo
        sr.Close();
        meuArquivo.Close();

        // Exibe as contagens
        Console.WriteLine("Existem {0} vogais e existem {1} consoantes", qtdVogais, qtdConsoantes);
        Console.WriteLine("O total de caracteres lidos foi {0}", qtdChar);

    }
}