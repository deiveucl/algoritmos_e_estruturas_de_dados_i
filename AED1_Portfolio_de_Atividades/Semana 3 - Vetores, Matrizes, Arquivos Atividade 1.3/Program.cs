﻿using System;
using System.IO;
using System.Text;

class MainClass {
    public static void Main(string[] args) {

        // Exercício 3: Crie um programa em C# que realize a leitura de 10 números presentes em um arquivo e exiba para o usuário a raiz quadrada de cada número lido.

        // Associando um objeto manipulador ao meu arquivo
        Console.WriteLine("Associando um objeto manipulador ao arquivo...");
        FileStream meuArquivo = new FileStream("numeros.txt", FileMode.Open, FileAccess.Read);

        // Usando meu arquivo como um fluxo de dados em memória
        Console.WriteLine("Usando o arquivo como fluxo de dados em memória...");
        StreamReader sr = new StreamReader(meuArquivo, Encoding.UTF8);

        // Lendo cada linha do arquivo e exibindo o número e sua raiz quadrada
        Console.WriteLine("Lendo todas as linhas e exibindo o valor calculado...\n...");
        while (!sr.EndOfStream) {
            float numeroAtual = float.Parse(sr.ReadLine());
            Console.WriteLine("A raiz quadrada de {0} é {1}", numeroAtual, Math.Sqrt(numeroAtual));
        }

        // Fechando StreamReader e FileStream
        Console.WriteLine("...\nFechando os acessos...");
        sr.Close();
        meuArquivo.Close();

    }
}