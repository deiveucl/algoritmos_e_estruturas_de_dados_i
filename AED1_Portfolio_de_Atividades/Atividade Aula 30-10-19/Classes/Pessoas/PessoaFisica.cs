class PessoaFisica : Pessoa {

    private string _cpf;
    private string _rg;

    public PessoaFisica(string nome, string dataNascFund, string cpf, string rg, int numero, string rua, string bairro, string cidade, string estado, string pais) {
        _nome = nome;
        _dataNascFund = dataNascFund;
        _cpf = cpf;
        _rg = rg;
        _numero = numero;
        _rua = rua;
        _bairro = bairro;
        _cidade = cidade;
        _estado = estado;
        _pais = pais;
    }

    public string GetCadastroInfo() {
        return "Nome: " + _nome +
               ". Data nasc: " + _dataNascFund +
               ". CPF: " + _cpf + 
               ". RG: " + _rg +
               "\n---- Endereço ----\n" +
               "Rua " + _rua + ". Nº " + _numero + ". Bairro " + _bairro +
               "\nCidade " + _cidade + ". Estado " + _estado + ". País " + _pais;
    }

}