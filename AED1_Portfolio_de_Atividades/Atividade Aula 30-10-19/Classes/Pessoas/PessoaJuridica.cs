class PessoaJuridica : Pessoa {

    private string _cnpj;
    private string _inscEstad;

    public PessoaJuridica(string nome, string dataNascFund, string cnpj, string inscEstad, int numero, string rua, string bairro, string cidade, string estado, string pais) {
        _nome = nome;
        _dataNascFund = dataNascFund;
        _cnpj = cnpj;
        _inscEstad = inscEstad;
        _numero = numero;
        _bairro = bairro;
        _cidade = cidade;
        _estado = estado;
        _pais = pais;
    }

    public string GetCadastroInfo() {
        return "Nome: " + _nome +
               ". Data nasc: " + _dataNascFund +
               ". CPF: " + _cnpj + 
               ". RG: " + _inscEstad +
               "\n---- Endereço ----\n" +
               "Rua " + _rua + ". Nº " + _numero + ". Bairro " + _bairro +
               "\nCidade " + _cidade + ". Estado " + _estado + ". País " + _pais;
    }

}