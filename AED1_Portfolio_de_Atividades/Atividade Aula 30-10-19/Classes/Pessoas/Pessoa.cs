public class Pessoa {

    // Identificação
    protected string _nome;
    protected string _dataNascFund;

    // Endereço
    protected int _numero;
    protected string _rua;
    protected string _bairro;
    protected string _cidade;
    protected string _estado;
    protected string _pais;

    public string GetNome() {
        return _nome;
    }

    public string GetDataNascFund() {
        return _dataNascFund;
    }

    public string GetEnderecoCompleto() {
        return "Rua " + _rua + ". Nº " + _numero + ". Bairro " + _bairro +
               "\nCidade " + _cidade + ". Estado " + _estado + ". País " + _pais;
    }

}