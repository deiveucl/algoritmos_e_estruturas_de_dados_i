public class Veiculo {

    protected string _marca;
    protected string _modelo;
    protected float _pesoMaximoSuportado;
    protected float _velocidadeMaxima;
    protected string _tipoDoVeiculo; // Ex. Transporte - Passeio - Corrida - etc

    public string GetInforBasicas() {
        return "Marca: " + _marca +
               ". Modelo: " + _modelo +
               ". Peso suportado: " + _pesoMaximoSuportado.ToString() + "Kg" +
               ". Vel. máxima: " + _velocidadeMaxima.ToString() + "Km/h" +
               ". Tipo do veículo: " + _tipoDoVeiculo;
    }

}