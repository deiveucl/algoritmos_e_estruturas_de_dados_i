class Bicicleta : Veiculo {

    public Bicicleta(string marca, string modelo, float pesoMaximoSuportado, float velocidadeMaxima, string tipoDoVeiculo) {
        _marca = marca;
        _modelo = modelo;
        _pesoMaximoSuportado = pesoMaximoSuportado;
        _velocidadeMaxima = velocidadeMaxima;
        _tipoDoVeiculo = tipoDoVeiculo;
    }

}