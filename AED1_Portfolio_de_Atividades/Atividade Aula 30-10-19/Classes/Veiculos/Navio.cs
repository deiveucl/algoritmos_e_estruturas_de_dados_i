class Navio : Veiculo {
    
    // Nome batizado ao navio
    private string _nomeDoNavio;

    public Navio(string nomeDoNavio, string marca, string modelo, float pesoMaximoSuportado, float velocidadeMaxima, string tipoDoVeiculo) {
        _nomeDoNavio = nomeDoNavio;
        _marca = marca;
        _modelo = modelo;
        _pesoMaximoSuportado = pesoMaximoSuportado;
        _velocidadeMaxima = velocidadeMaxima;
        _tipoDoVeiculo = tipoDoVeiculo;
    }

    public string GetInfoNavio() {
        return "Nome: " + _nomeDoNavio +
               ". Marca: " + _marca +
               ". Modelo: " + _modelo +
               ". Peso suportado: " + _pesoMaximoSuportado.ToString() + "Kg" +
               ". Vel. máxima: " + _velocidadeMaxima.ToString() + "Km/h" +
               ". Tipo do veículo: " + _tipoDoVeiculo;
    }

}