public class Conta {
    protected int _numero;
    protected string _titular;
    protected double _saldo;

    public void Sacar(double valor) {
        if (valor <= _saldo) {
            _saldo -= valor;
        }
    }

    public void Depositar(double valor) {
        if (valor >= 0) {
            _saldo += valor;
        }
    }

    public string GetInformacoes() {
        return "Titular: " + _titular + " - Nº da Conta: " + _numero;
    }

}