class ContaPoupanca : Conta {
    private double _rendimento;
    public double Rendimento {
        get {
            return Rendimento;
        }
        set {
            if (value > 0) {
                _rendimento = value;
            }
        }
    }
}