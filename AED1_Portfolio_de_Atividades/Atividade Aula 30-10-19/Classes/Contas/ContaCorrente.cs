class ContaCorrente : Conta {
    private double _taxaManutencao;
    public double TaxaManutencao {
        get {
            return _taxaManutencao;
        }
        set {
            _taxaManutencao = value;
        }
    }
}