﻿using System;

class MainClass {
    public static void Main(string[] args) {

        Console.Write("Insira seu nome completo: ");
        string nomeCompleto = Console.ReadLine();
        Console.Write("Insira sua data de nascimento no seguinte formato [DD/MM/AAAA]: ");
        string dataNascimento = Console.ReadLine();
        Console.Write("Insira seu CPF no seguinte formato [xxx.xxx.xxx-xx]: ");
        string cpf = Console.ReadLine();
        Console.Write("Insira seu RG no seguinte formato [xxxxxxx]: ");
        string rg = Console.ReadLine();
        Console.Write("Insira o nome da rua: ");
        string rua = Console.ReadLine();
        Console.
        
        PessoaFisica eu = new PessoaFisica("Deivisson Altoé Ferreira", "03/10/1995", "143.205.317-52", "3260799", 272, "Copacabana", "Jardim Guanabara", "Serra", "Espírito Santo", "Brasil");
        
        Console.WriteLine("Nome completo: " + eu.GetNome());
        Console.WriteLine("Data de nascimento " + eu.GetDataNascFund());
        Console.WriteLine("Endereço: " + eu.GetEnderecoCompleto());

        Console.WriteLine(); // Pular linha

        Console.WriteLine(eu.GetCadastroInfo());

        Console.WriteLine(); // Pular linha

        Bicicleta bicibike = new Bicicleta("Caloi", "HX-2", 350f, 40f, "Casual Adulto");
        Console.WriteLine(bicibike.GetInforBasicas());

        Console.WriteLine(); // Pular linha

        Moto motinha = new Moto("Suzuki", "SZK6", 400f, 350f, "Corrida Prof");
        Console.WriteLine(motinha.GetInforBasicas());

        Console.WriteLine(); // Pular linha

        Navio navio = new Navio("Haloween dos Hallo", "No idea..", "Same..", 10800f, 150f, "Cargueiro");
        Console.WriteLine(navio.GetInfoNavio());
        
    }
}