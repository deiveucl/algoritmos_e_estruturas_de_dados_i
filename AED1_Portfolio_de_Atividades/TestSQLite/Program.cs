﻿using System;
using System.Data.SQLite;

namespace TestSQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = "Data Source=teste_db.db";
            string stm = "SELECT * FROM pessoa";

            using var con = new SQLiteConnection(cs);
            con.Open();

            using var cmd = new SQLiteCommand(stm, con);
            string version = cmd.ExecuteScalar().ToString();

            Console.WriteLine($"SQLite version: {version}");
        }
    }
}
