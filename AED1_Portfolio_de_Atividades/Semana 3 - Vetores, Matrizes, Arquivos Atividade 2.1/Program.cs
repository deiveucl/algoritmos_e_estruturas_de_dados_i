﻿using System;
using System.IO;

class MainClass {
    public static void Main(string[] args) {

        // Exercício 1: Crie um programa em C# que realize a leitura de números inteiros positivos (informados pelo usuário) e grave em um arquivo apenas os números múltiplos de 7. O programa deve parar quando o usuário informar um número negativo.

        // Associando um objeto manipulador ao arquivo para escrita
        StreamWriter sw = new StreamWriter("multiplosDeSete.txt");

        // Declarando uma variável para receber números do tipo inteiro do usuário
        int numeroUsuario;

        // Fazer essa repetição enquanto usuário não digitar um número negativo
        do {
            Console.Write("Digite um número inteiro. Para parar, digite um número negativo: ");
            numeroUsuario = int.Parse(Console.ReadLine());

            if (numeroUsuario % 7 == 0) {
                sw.WriteLine(numeroUsuario);
            }
        } while (numeroUsuario >= 0);


        // Fecha o acesso ao arquivo
        sw.Close();
    }
}