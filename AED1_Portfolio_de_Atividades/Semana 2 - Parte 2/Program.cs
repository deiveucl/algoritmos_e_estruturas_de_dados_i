﻿using System;

class Program {
    static void Main(string[] args) {
        
        Console.Write("Insira o primeiro número: ");
        int num_1 = int.Parse(Console.ReadLine());
        
        Console.Write("Insira o segundo número: ");
        int num_2 = int.Parse(Console.ReadLine());
        
        Console.Write("Insira o terceiro número: ");
        int num_3 = int.Parse(Console.ReadLine());
        
        int maior = num_1;
        
        if (num_2 > maior) {
            maior = num_2;
        }
        
        if (num_3 > maior) {
            maior = num_3;
        }

        Console.WriteLine("O maior número é {0}", maior);

    }
}
