﻿using System;


class Alienigena {

    private string nome;
    private static string mensagem;

    public Alienigena(string nome) {
        this.nome = nome;
    }

    public void getNome() {
        Console.WriteLine(nome + ". " + mensagem);
    }

    public void getMensagem() {
        Console.WriteLine(mensagem);
    }

    public static void setMensagem(string novaMensagem) {
        mensagem = novaMensagem;
    }

}

class Program {

    static void Main(string[] args) {
        Alienigena et_1 = new Alienigena("Astolfo");
        Alienigena et_2 = new Alienigena("João");

        Alienigena.setMensagem("Somos todos alienigenas!");

        et_1.getNome();
        et_2.getNome();

        et_1.getMensagem();
        et_2.getMensagem();
    }
    
}