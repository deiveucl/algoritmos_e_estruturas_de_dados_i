﻿using System;

class MainClass {
  public static void Main(string[] args) {
    /*
    Exercício 2 - Crie uma classe para representar uma Data que possua:
    - Pelo menos 2 construtores
    - Os métodos GET e SET
    - Implemente o método público NomeMes() que retorna o nome do mês atualmente configurado em um objeto da classe Data
    - Implemente o método Bissexto() que retorna verdadeiro se o ano atual for bissexto, e retorna falso caso contrário
    - Implemente o método Incrementa(int dias) da classe Data. Esse método adiciona à data atual o número de dias informado como parâmetro
    - Implemente o método Diferenca() que retorna o número de dias entre duas datas (a atual e uma outra informada como parâmetro)
    */

    // Instanciando um objeto
    Data hoje = new Data(20, 2, 2088);

    // Usando métodos GET
    Console.WriteLine("{0}", hoje.getDia());
    Console.WriteLine("{0}", hoje.getMes());
    Console.WriteLine("{0}", hoje.getAno());
    Console.WriteLine("{0}", hoje.getNomeMes());
    Console.WriteLine("{0}", hoje.getDataCompleta());
    Console.WriteLine("{0}", hoje.getVerAnoBis());
    Console.WriteLine("{0}", hoje.dataVerif());

    hoje.incrementaDia(1);
    Console.WriteLine("{0}", hoje.getDataCompleta());

    Data nova_data = new Data(20, 1, 2088);
    Console.WriteLine("{0}", hoje.diferencaData(nova_data));

  }
}