class Data {

    // Declarações dos atributos
    private int dia;
    private int mes;
    private int ano;
    private bool bissexto = false;
    private int[] qtdDiasMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // Contrutor com valores padrão
    public Data() {
        dia = 3;
        mes = 10;
        ano = 1995;
        setAnoBissexto(ano);
    }

    // Construtor com parâmetros
    public Data(int dia, int mes, int ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        setAnoBissexto(this.ano);
    }

    // Atribui o valor referente ao dia
    public void setDia(int dia) {
        this.dia = dia;
    }
    // Atribui o valor referente ao mes
    public void setMes(int mes) {
        this.mes = mes;
    }
    // Atribui o valor referente ao ano
    public void setAno(int ano) {
        this.ano = ano;
    }

    // Verifica se o ano é bissexto ou não
    private void setAnoBissexto(int ano) {
        if ((ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0)) {
            qtdDiasMes[1] = 29;
            bissexto = true;
        }
    }

    // Verificador da data
    public string dataVerif() {
        if (mes >= 1 && mes <= 12) {
            if (dia >= 1 && dia <= qtdDiasMes[mes - 1]) {
                return "Data válida!!";
            }
            return "Data inválida! O dia está errado!";
        }
        return "Data inválida! O mês está errado!";
    }

}