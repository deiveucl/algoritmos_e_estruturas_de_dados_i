﻿using System;
class Program {
    static void Main(string[] args) {

        /*
        Dados dois vetores, A (5 elementos) e B (8 elementos), faça um programa em C# que imprima todos os elementos comuns aos dois vetores.
        */

        // Declarando os dois vetores
        string[] vetorA = new string[5];
        string[] vetorB = new string[8];

        // Preenchendo os vetores de um jeito nada a ver...
        for (int i = 0; i < 8; i++) {
            vetorB[i] = i.ToString();
            if (i < 5) {
                vetorA[i] = i.ToString();
            }
        }

        Console.WriteLine("Os valores em comum são os seguintes:");
        foreach (string posicao_a in vetorA) {
            foreach (string posicao_b in vetorB) {
                if (posicao_a == posicao_b) {
                    Console.WriteLine("{0}.", posicao_a);
                }
            }
        }

    }
}