﻿using System;
using System.IO;
using System.Text;

class MainClass {
    public static void Main(string[] args) {
        

        // Exercício 1: Crie um programa em C# que realize a leitura de 2 números presentes em um arquivo e, em seguida, exiba para o usuário a soma dos dois números lidos.

        // Associando um objeto manipulador ao meu arquivo
        Console.WriteLine("Associando arquivo ao objeto manipulador...");
        FileStream meuArq = new FileStream("numeros.txt", FileMode.Open, FileAccess.Read);

        // Usando o arquivo associado como um fluxo de dados em memória
        Console.WriteLine("Usando arquivo associado como fluxo de dados em memória...");
        StreamReader sr = new StreamReader(meuArq, Encoding.UTF8);

        // Em sequência, lendo o primeiro e segundo número presente no arquivo
        Console.WriteLine("Lendo a primeira linha...");
        float numero_1 = float.Parse(sr.ReadLine());
        Console.WriteLine("Lendo a segunda linha...");
        float numero_2 = float.Parse(sr.ReadLine());

        // Fechando fluxo em memória e o arquivo
        Console.WriteLine("Fechando os acessos abertos...");
        sr.Close();
        meuArq.Close();

        // Exibindo os números e a soma entre eles
        Console.WriteLine("...\nA soma entre os números {0} e {1} é igual a {2}\n...", numero_1, numero_2, numero_1 + numero_2);

    }
}