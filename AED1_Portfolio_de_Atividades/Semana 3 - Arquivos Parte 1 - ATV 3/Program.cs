﻿using System;
using System.IO;
using System.Text;

class Program {
    static void Main(string[] args) {
        
        // Associando um objeto manipulador ao meu arquivo
        FileStream meuArquivo = new FileStream("dados.txt", FileMode.Open, FileAccess.Read);

        // Usando o arquivo associado como fluxo de dados em memória
        StreamReader sr = new StreamReader(meuArquivo, Encoding.UTF8);

        // Lendo os números e calculando
        while (!sr.EndOfStream) {

            // Armazena o valor
            float numero = float.Parse(sr.ReadLine());

            // Exibe o valor o resultado do calculo
            Console.WriteLine("A raiz quadrada de {0} é igual a {1}", numero, Math.Sqrt(numero));

        }

        // Fechando o arquivo
        sr.Close();
        meuArquivo.Close();

    }
}