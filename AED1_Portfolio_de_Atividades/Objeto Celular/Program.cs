﻿using System;

class Program {
    static void Main(string[] args) {
        
        // Objeto CELULAR
        Celular celular_1 = new Celular();
        Console.WriteLine(celular_1.getCelularCompleto());

        celular_1.setQtdRam(6);
        celular_1.setQtdArmazenamento(64);
        celular_1.setTela(6.3);
        celular_1.setMarca("iPhone");
        celular_1.setModelo("Samsung A9");

        Console.WriteLine(celular_1.getCelularCompleto());

        Console.WriteLine();

        Celular celular_2 = new Celular(3, 32, 5, "Xiaomi", "Redmi 4x");
        Console.WriteLine(celular_2.getCelularCompleto());

    }
}