class Celular {

    private int qtdRam;
    private int qtdArmazenamento;
    private double tamanhoTela;
    private string marca;
    private string modelo;

    public Celular() {
        qtdRam = 1;
        qtdArmazenamento = 8;
        tamanhoTela = 4;
        marca = "Samsung";
        modelo = "A10";
    }

    public Celular(int qtdRam, int qtdArmazenamento, float tamanhoTela, string marca, string modelo) {
        this.qtdRam = qtdRam;
        this.qtdArmazenamento = qtdArmazenamento;
        this.tamanhoTela = tamanhoTela;
        this.marca = marca;
        this.modelo = modelo;
    }

    // Métodos SET
    public void setQtdRam(int quantidadeRam) {
        this.qtdRam = quantidadeRam;
    }
    public void setQtdArmazenamento(int quantidadeArmazenamento){
        this.qtdArmazenamento = quantidadeArmazenamento;
    }
    public void setTela(double tamanhoTela){
        this.tamanhoTela = tamanhoTela;
    }
    public void setMarca(string marca){
        this.marca = marca;
    }
    public void setModelo(string modelo){
        this.modelo = modelo;
    }

    public bool getClassificacaoTela(){
        if(tamanhoTela > 6){
        return true;
        }
        else{
        return false;
        }
    }

    public string getCelularCompleto() {
        return "A marca do celular é " + marca + " e seu modelo é " + modelo + ". Tela de " + tamanhoTela.ToString() + " polegadas. Armazenamento de " + qtdArmazenamento.ToString() + " GB e "+ qtdRam.ToString() + "GB de RAM" + "Esse celular é consideravel grande: " + getClassificacaoTela().ToString();
    }

}