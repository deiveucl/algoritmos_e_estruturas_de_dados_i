﻿using System;

class MainClass {
    public static void Main(string[] args) {

        // Crie uma classe para representar o tipo “caneta”.

        // Instanciando minha classe caneta, passando como parâmetro seus dados utilizando o método construtor
        Caneta caneta1 = new Caneta("BIC", 100, "Preta", "Fina", true);

        // Usando os métodos públicos GET para pegar as variáveis privadas da classe
        Console.WriteLine("Marca: {0}", caneta1.getMarca());
        Console.WriteLine("Nível de Tinta: {0}%", caneta1.getNivelDeTinta());
        Console.WriteLine("Cor: {0}", caneta1.getCor());
        Console.WriteLine("Tipo da Ponta: {0}", caneta1.getTipoDaPonta());
        Console.WriteLine("Status da Tampa: {0}", caneta1.getStatusTampa());

        // Utilizando métodos públicos SET para atribuir novos valores às variáveis privadas da instância
        caneta1.setMarca("Faber Castell");
        caneta1.setNivelDeTinta(55);
        caneta1.setCor("Azul");
        caneta1.setTipoDaPonta("Grossa");
        caneta1.setDestamparCaneta();

        // Exibindo novamente os dados com os métodos GET
        Console.WriteLine("\nMarca: {0}", caneta1.getMarca());
        Console.WriteLine("Nível de Tinta: {0}%", caneta1.getNivelDeTinta());
        Console.WriteLine("Cor: {0}", caneta1.getCor());
        Console.WriteLine("Tipo da Ponta: {0}", caneta1.getTipoDaPonta());
        Console.WriteLine("Status da Tampa: {0}", caneta1.getStatusTampa());

    }
}