class Caneta {

    private string marca;
    private int nivelDeTinta;
    private string cor;
    private string tipoDaPonta;
    private bool tampada;


    public Caneta(string m, int niv, string c, string tipPont, bool tamp) {
        marca = m;
        nivelDeTinta = niv;
        cor = c;
        tipoDaPonta = tipPont;
        tampada = tamp;
    }


    public string getMarca() {
        return marca;
    }
    public int getNivelDeTinta() {
        return nivelDeTinta;
    }
    public string getCor() {
        return cor;
    }
    public string getTipoDaPonta() {
        return tipoDaPonta;
    }
    public string getStatusTampa() {
        if (tampada == true) {
            return "A caneta está tampada!";
        } else {
            return "A caneta está destampada!";
        }
    }


    public void setMarca(string m) {
        this.marca = m;
    }
    public void setNivelDeTinta(int niv) {
        this.nivelDeTinta = niv;
    }
    public void setCor(string c) {
        this.cor = c;
    }
    public void setTipoDaPonta(string tipPont) {
        this.tipoDaPonta = tipPont;
    }
    public void setTamparCaneta() {
        this.tampada = true;
    }
    public void setDestamparCaneta() {
        this.tampada = false;
    }



    public void tamparCaneta() {
        this.tampada = true;
    }
    public void destamparCaneta() {
        this.tampada = false;
    }

    // Operações
    // Transferir carga
    // Escrever
    // Destampar
    // Tampar

}