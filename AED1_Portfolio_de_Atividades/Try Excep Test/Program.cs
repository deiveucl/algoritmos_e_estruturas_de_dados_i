﻿using System;

class Program {
    static void Main(string[] args) {
        


        int num;

        while (true) {

            Console.Write("Insira um número inteiro qualquer: ");
            
            try {
                num = int.Parse(Console.ReadLine());
                break;
            } catch (FormatException) {
                Console.WriteLine("Número inválido! Tente novamente...");
            }
        
        }

        Console.WriteLine("O número informado foi: {0}", num);


    }
}
