using System;

class Triangulo {

    private double a;
    private double b;
    private double c;

    public Triangulo(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    private double _valorP() {
        return (this.a + this.b + this.c) / 2;
    }

    public double _areaTriangulo() {
        return Math.Sqrt(_valorP() * (_valorP() - this.a) * (_valorP() - this.b) * (_valorP() - this.c));
    }

    public static string _maiorArea(Triangulo x, Triangulo y) {
        if (x._areaTriangulo() > y._areaTriangulo()) {
            return "X";
        } else {
            return "Y";
        }
    }

}