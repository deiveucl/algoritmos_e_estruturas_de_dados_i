﻿using System;

class Program {
    public static void Main(string[] arrgs) {

        Console.WriteLine("Entre com as medidas do triângulo X: ");
        
        double a = double.Parse(Console.ReadLine());
        double b = double.Parse(Console.ReadLine());
        double c = double.Parse(Console.ReadLine());

        Triangulo tX = new Triangulo(a, b, c);

        Console.WriteLine("Entre com as medidas do triângulo Y: ");

        a = double.Parse(Console.ReadLine());
        b = double.Parse(Console.ReadLine());
        c = double.Parse(Console.ReadLine());

        Triangulo tY = new Triangulo(a, b, c);

        Console.WriteLine("Área de X: {0}", tX._areaTriangulo());
        Console.WriteLine("Área de Y: {0}", tY._areaTriangulo());
        Console.WriteLine("Maior Área: {0}", Triangulo._maiorArea(tX, tY));
        
    }
}