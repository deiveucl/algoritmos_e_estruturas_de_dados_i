﻿using System;

class Program {

    public static int RestoDaDivisao(int dividendo, int divisor) {
        int resto = dividendo - divisor;
        if (resto < divisor) {
            return resto;
        }
        return RestoDaDivisao(resto, divisor);
    }

    /*
        15 - 4 = 11
        11 - 4 = 7
        7 - 4 = 3
    */

    // Crie um programa em C# que calcule o resto da divisão inteira entre dois números por meio de sucessivas subtrações

    public static void Main(string[] args) {
            
        // Obtendo dados do usuário - NUMERADOR
        Console.Write("\nNumerador inteiro: ");
        int numerador = int.Parse(Console.ReadLine());
        // Obtendo dados do usuário - DENOMINADOR
        Console.Write("Denominador inteiro: ");
        int denominador = int.Parse(Console.ReadLine());

        // Exibindo resultado modelo para verificação
        Console.WriteLine("\nUsando % - {0} % {1} = {2}", numerador, denominador, numerador % denominador);

        // Exibindo resultado calculado pelo método criado acima
        Console.WriteLine("\nO resto da divisão de {0} por {1} é igual a {2}", numerador, denominador, RestoDaDivisao(numerador, denominador));

    }

}