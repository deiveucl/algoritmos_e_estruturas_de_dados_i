class Vagao {

    // Atributos privados
    private string descricao_carga;
    private float carga_maxima_ton;
    private float qtd_material_ton;

    // Contrutor padrão
    public Vagao() {
        this.descricao_carga = "Nada";
        this.carga_maxima_ton = 1000f;
        this.qtd_material_ton = 0f;
    }

    // Construtor com parâmetros
    public Vagao(string descricao_carga, float carga_maxima_ton, float qtd_material_ton) {
        this.descricao_carga = descricao_carga;
        this.carga_maxima_ton = carga_maxima_ton;
        this.qtd_material_ton = qtd_material_ton;
    }

    // Métodos SET
    public void SetDescricaoCarga(string descricao_carga) {
        this.descricao_carga = descricao_carga;
    }
    public void SetCargaMaximaTon(float carga_maxima_ton) {
        this.carga_maxima_ton = carga_maxima_ton;
    }
    public void SetQtdMaterialTon(float qtd_material_ton) {
        this.qtd_material_ton = qtd_material_ton;
    }

    // Métodos GET
    public string GetDescricaoCarga() {
        return this.descricao_carga;
    }
    public float GetCargaMaximaTon() {
        return this.carga_maxima_ton;
    }
    public float GetQtdMaterialTon() {
        return this.qtd_material_ton;
    }

}