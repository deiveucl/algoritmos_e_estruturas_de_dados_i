﻿using System;

class Program {
    static void Main(string[] args) {
        
        // Testando construtor padrão
        Trem tabajara = new Trem();
        Console.WriteLine(tabajara.GetNomeTrem());
        Console.WriteLine(tabajara.GetVelocidadeMaxima());
        Console.WriteLine(tabajara.GetCargaUsada());
        Console.WriteLine(tabajara.GetCargaMaxima());
        Console.WriteLine(tabajara.GetCapacidadeDisponivel());

        // Testando construtor com parâmetros
        Vagao vagao_1 = new Vagao("Metais Preciosos", 5000f, 4000f);
        Vagao vagao_2 = new Vagao("Madeiras", 6000f, 1000f);
        Vagao vagao_3 = new Vagao("Água", 10210f, 500f);
        Trem jamelao = new Trem("Jamelão", 250f, vagao_1, vagao_2, vagao_3);

        Console.WriteLine(jamelao.GetNomeTrem());
        Console.WriteLine(jamelao.GetVelocidadeMaxima());
        Console.WriteLine(jamelao.GetCargaUsada());
        Console.WriteLine(jamelao.GetCargaMaxima());
        Console.WriteLine(jamelao.GetCapacidadeDisponivel());

    }
}