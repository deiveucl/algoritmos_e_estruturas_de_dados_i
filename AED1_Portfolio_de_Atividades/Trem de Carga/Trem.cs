class Trem {

    // Atributos privados
    private string nome_trem;
    private float velocidade_maxima_trem;
    private float carga_maxima_trem;
    private float carga_usada_trem;
    private Vagao vagao1;
    private Vagao vagao2;
    private Vagao vagao3;

    // Construtor padrão
    public Trem() {
        this.nome_trem = "Trem padrão";
        this.velocidade_maxima_trem = 100f;
        this.vagao1 = new Vagao();
        this.vagao2 = new Vagao();
        this.vagao3 = new Vagao();
        this.carga_maxima_trem = GetCargaMaximaVagoes(this.vagao1, this.vagao2, this.vagao3);
        this.carga_usada_trem = GetCargaUsadaVagoes(this.vagao1, this.vagao2, this.vagao3);
    }

    // Construtor com parâmetros
    public Trem(string nome_trem, float velocidade_maxima_trem, Vagao vagao1, Vagao vagao2, Vagao vagao3) {
        this.nome_trem = nome_trem;
        this.velocidade_maxima_trem = velocidade_maxima_trem;
        this.vagao1 = vagao1;
        this.vagao2 = vagao2;
        this.vagao3 = vagao3;
        this.carga_maxima_trem = GetCargaMaximaVagoes(vagao1, vagao2, vagao3);
        this.carga_usada_trem = GetCargaUsadaVagoes(vagao1, vagao2, vagao3);
    }

    // Retorna nome do trem 
    public string GetNomeTrem() {
        return this.nome_trem;
    }
    // Retorna velocidade máxima do trem
    public float GetVelocidadeMaxima() {
        return this.velocidade_maxima_trem;
    }
    // Retorna a carga máxima suportada pelo trem
    public float GetCargaMaxima() {
        return this.carga_maxima_trem;
    }
    // Retorna a carga usada no trem
    public float GetCargaUsada() {
        return this.carga_usada_trem;
    }

    // Retorna quantidade máxima que o trem suporta de carga
    private float GetCargaMaximaVagoes(Vagao vagao1, Vagao vagao2, Vagao vagao3) {
        float total = vagao1.GetCargaMaximaTon() + vagao2.GetCargaMaximaTon() + vagao3.GetCargaMaximaTon();

        return total;
    }
    // Retorna o total de carga ocupada no trem
    private float GetCargaUsadaVagoes(Vagao vagao1, Vagao vagao2, Vagao vagao3) {
        float total = vagao1.GetQtdMaterialTon() + vagao2.GetQtdMaterialTon() + vagao3.GetQtdMaterialTon();

        return total;
    }
    // Retorna capacidade ainda disponível
    public float GetCapacidadeDisponivel() {
        return this.carga_maxima_trem - this.carga_usada_trem;
    }

}