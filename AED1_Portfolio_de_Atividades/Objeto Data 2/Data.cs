class Data {

    // Declarações dos atributos
    private int dia;
    private int mes;
    private int ano;
    private bool bissexto = false;
    private int[] qtdDiasMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};


    // Contrutor com valores padrão
    public Data() {
        dia = 1;
        mes = 9;
        ano = 1995;
        setAnoBissexto(ano);
    }


    // Construtor com valores do programador instanciando um objeto
    public Data(int dia, int mes, int ano) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        setAnoBissexto(this.ano);
    }


    // Métodos SET
    // Atribui o valor referente ao dia
    public void setDia(int dia) {
        this.dia = dia;
    }
    // Atribui o valor referente ao mes
    public void setMes(int mes) {
        this.mes = mes;
    }
    // Atribui o valor referente ao ano
    public void setAno(int ano) {
        this.ano = ano;
    }


    // Métodos GET
    // Retorna uma string com o número do dia
    public string getDia() {
        if (dia >= 0 && dia <= 9) {
        return "0" + dia.ToString();
        }
        return dia.ToString();
    }
    // Retorna uma string com o número do mês
    public string getMes() {
        if (mes >= 1 && mes <= 9) {
        return "0" + mes.ToString();
        }
        return mes.ToString();
    }
    // Retorna um número inteiro com o valor do ano
    public int getAno() {
        return ano;
    }
    // Retorna uma string dizendo se o ano é bissexto ou não
    public string getVerAnoBis() {
        if (bissexto == true) {
        return "O ano " + ano.ToString() + " é bissexto";
        }
        return "O ano " + ano.ToString() + " não é bissexto";
    }
    // Retorna uma string formatada com a data DD/MM/AAAA
    public string getDataCompleta() {
        return getDia() + "/" + getMes() + "/" + ano.ToString();
    }


    // Retorna o nome do mês da data
    public string getNomeMes() {
        switch (mes) {
            case 1: return "Janeiro";
            case 2: return "Fevereiro";
            case 3: return "Março";
            case 4: return "Abril";
            case 5: return "Maio";
            case 6: return "Junho";
            case 7: return "Julho";
            case 8: return "Agosto";
            case 9: return "Setembro";
            case 10: return "Outubro";
            case 11: return "Novembro";
            case 12: return "Dezembro";
            default: return "Mês não declarado";
        }
    }


    // Verifica se o ano é bissexto ou não
    public void setAnoBissexto(int ano) {
        if ((ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0)) {
            qtdDiasMes[1] = 29;
            bissexto = true;
        }
    }


    // Verificador da data
    public string dataVerif() {
        if (mes >= 1 && mes <= 12) {
            if (dia >= 1 && dia <= qtdDiasMes[mes - 1]) {
                return "Data válida!!";
            }
            return "Data inválida! O dia está errado!";
        }
        return "Data inválida! O mês está errado!";
    }


    // Método incrementa dias
    public void incrementaDia(int qtdDias) {
        int contador = 0;
        do {
            if (dia < qtdDiasMes[mes - 1]) {
                dia = dia + 1;
            } else {
                dia = 1;
                if (mes == 12) {
                ano = ano + 1;
                setAnoBissexto(ano);
                mes = 1;
                } else {
                mes = mes + 1;
                }
            }
            contador = contador + 1;
        } while (contador < qtdDias);
    }

    
    // Método diferença de dias entre datas
    public string diferencaData(Data outraData) {

        string dataCompletaOutra = outraData.getDataCompleta();
        int contador = 0;

        while (this.getDataCompleta() != outraData.getDataCompleta()) {
            outraData.incrementaDia(1);
            contador = contador + 1;
        }

        return "A diferença é de " + contador.ToString() + " dias entre " + this.getDataCompleta() + " e " + dataCompletaOutra;
    }

}