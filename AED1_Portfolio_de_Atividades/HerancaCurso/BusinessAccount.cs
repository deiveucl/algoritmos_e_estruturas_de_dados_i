class BusinessAccount : Account {

    private double loanLimit;

    public BusinessAccount(int number, string holder, double balanceLimit, double loanLimit) {
        this.number = number;
        this.holder = holder;
        this.balanceLimit = balanceLimit;
        this.loanLimit = loanLimit;
        this.balance = 0;
    }

    public bool loan(double amount) {
        if (amount <= loanLimit) {
            return true;
        } else {
            return false;
        }
    }

}