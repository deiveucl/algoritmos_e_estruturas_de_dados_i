class Account {

    protected double number;
    protected string holder;
    protected double balance;
    protected double balanceLimit;

    protected bool Withdraw(double amount) {
        if (balance >= amount) {
            balance -= amount;
            return true;
        } else {
            return false;
        }
    }

    protected bool Deposit(double amount) {
        if ((amount + balance) <= balanceLimit) {
            balance += amount;
            return true;
        } else {
            return false;
        }
    }

    protected double getBalance() {
        return balance;
    }

}