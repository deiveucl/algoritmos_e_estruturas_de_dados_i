class SavingsAccount : Account {

    public SavingsAccount(int number, string holder,double balanceLimit) {
        this.number = number;
        this.holder = holder;
        this.balanceLimit = balanceLimit;
        this.balance = 0;
    }

}